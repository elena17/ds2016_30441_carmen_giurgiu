package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import java.io.IOException;

import common.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.TextFileService;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		MailService mailService = new MailService("c.lenagiurgiu@gmail.com","******");
		TextFileService textFileService = new TextFileService();
		
		String message;
		DVD dvd;

		while(true) {
			try {
				dvd = queue.readMessage();
				message = dvd.getTitle() + " - " + dvd.getYear() + " - " + dvd.getPrice();
				
				System.out.println("Sending mail for " + dvd.getTitle());
				mailService.sendMail("c.lenagiurgiu@gmail.com",dvd.getTitle() + " was added",message);
				
				System.out.println("Creating text file for " + dvd.getTitle());
				textFileService.createTextFile(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
