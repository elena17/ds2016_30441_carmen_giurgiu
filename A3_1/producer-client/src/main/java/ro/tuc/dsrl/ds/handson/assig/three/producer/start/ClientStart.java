package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import java.io.IOException;
import java.util.Random;

import common.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		DVD dvdToSend;
		
		try {
			for (int i=1;i<6;i++) {
				int year = randInt(1960, 2016);
                                double p1 = randInt(10, 20);
                                double p2 = randInt(10, 99);
                                double price = p1 + p2/100;
                                
				
				dvdToSend = new DVD("DVD " + i, year, price);
				
				queue.writeMessage(dvdToSend);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
}
