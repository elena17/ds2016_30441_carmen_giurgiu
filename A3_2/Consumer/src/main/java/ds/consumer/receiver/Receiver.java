package ds.consumer.receiver;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;


import ds.consumer.mailservice.MailService;
import common.DVD;

public class Receiver {
	  private final static String QUEUE_NAME = "DVD";

	  public static void main(String[] argv)
	      throws java.io.IOException, TimeoutException {
	      
		  
		  ConnectionFactory factory = new ConnectionFactory();
		    factory.setHost("localhost");
		    Connection connection = factory.newConnection();
		    Channel channel = connection.createChannel();
		    
		    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		    System.out.println("Server started!");

		    Consumer consumer = new DefaultConsumer(channel) {
			      @Override
			      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			          throws IOException {                                  
                                  
			    	  
			    	  DVD dvd = new DVD("NULL", 0, 0);
			    	  String message = new String(body, "UTF-8");
			    	  try {
						     byte b[] = message.getBytes(); 
						     ByteArrayInputStream bias = new ByteArrayInputStream(b);
						     ObjectInputStream si = new ObjectInputStream(bias);
						     dvd = (DVD) si.readObject();
						     
						    System.out.println("Received '" + dvd.getTitle() + "'");
						     
						     MailService mailService = new MailService("c.lenagiurgiu@gmail.com", "Elena001");
							
						     mailService.sendMail("c.lenagiurgiu@gmail.com", "New DVD was added!", dvd.print());
								
						     
						     PrintWriter writer = new PrintWriter(dvd.getTitle() + ".txt", "UTF-8");
						     writer.println(dvd.print());
						     writer.close();
						     
						 } catch (Exception e) {
						     System.out.println(e);
						 }
			        
			      }
			    };
			    channel.basicConsume(QUEUE_NAME, true, consumer);
			  }

	
	  
}
	