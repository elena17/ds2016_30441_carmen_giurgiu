package ds.producer.sender;


import common.DVD;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Sender {
	private final static String QUEUE_NAME = "DVD";
	
	public Sender () {
	}

	  public void sendDVD (String title, String year, String price) throws IOException, TimeoutException {
		  	ConnectionFactory factory = new ConnectionFactory();
		    factory.setHost("localhost");
		    Connection connection = factory.newConnection();
		    Channel channel = connection.createChannel();
		    
		    DVD dvd = new DVD(title, Integer.parseInt(year), Double.parseDouble(price));
		    String serializedObjectAsMessageContent = "";
		    
		    try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream  oos = new ObjectOutputStream(baos);
				oos.writeObject(dvd);
				oos.flush();
				serializedObjectAsMessageContent = baos.toString();

				//System.out.println("New DVD produced ");
				
			
			} catch (Exception e) {
			     System.out.println(e);
			}
		    
		    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		    channel.basicPublish("", QUEUE_NAME, null, serializedObjectAsMessageContent.getBytes("UTF-8"));
		    //channel.basicPublish(QUEUE_NAME, "", null, serializedObjectAsMessageContent.getBytes("UTF-8"));
		    System.out.println("Sent '" + dvd.getTitle()  + "'");
		    
		    channel.close();
		    connection.close();
	  }
}
