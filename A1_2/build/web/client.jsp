<%-- 
    Document   : admin
    Created on : 12.11.2016, 16:18:23
    Author     : vaio
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="entities.Users"%>
<%@ page import="entities.Flights"%>
<%@ page import="java.util.List"%>
<% Users user = (Users)session.getAttribute("user"); 
	if (user == null) {
		System.out.println("User is null...");
		response.sendRedirect("login.jsp");
		return;
	} else if (user.isIsAdmin()) {
		response.sendRedirect("admin.jsp");
		return;
	}

   List<Flights> clientFlights = (List)session.getAttribute("clientFlights");
   
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="client.js"></script>
	<style>
		#logout {
			float: right;
		}
                
                body{
                    background-color: rgb(220,220,255);                    
                    
                }
                
	</style>
	<title>User panel</title>
</head>
<body>
	<a id="logout" href="login.jsp">LOGOUT</a>
	<p>Welcome, <%=user.getUsername() %>!</p>
	<p>You have <%=clientFlights.size() %> flights:</p>
	<table border="1" id="flights">
		<tr>
			<th>Flight number</th>
			<th>Airplane type</th>
			<th>Departure city</th>
			<th>Departure date</th>
			<th>Departure time</th>
			<th>Arrival city</th>
			<th>Arrival date</th>
			<th>Arrival time</th>
		</tr>
		<% for (Flights flight : clientFlights) { %>
			<tr>
				<td><%=flight.getFlightNumber() %></td>
				<td><%=flight.getAirplaneType() %></td>
				<td><%=flight.getDepartureCity() %>&nbsp;
					<a href="" onclick="displayTime(<%=flight.getDepartureLatitude() %>,<%=flight.getDepartureLongitude() %>); return false;">
					See local time
					</a>
				</td>
				<td><%=flight.getDepartureDate() %></td>
				<td><%=flight.getDepartureTime() %></td>
				<td><%=flight.getArrivalCity() %>&nbsp;
					<a href="" onclick="displayTime(<%=flight.getArrivalLatitude() %>,<%=flight.getArrivalLongiture() %>); return false;">
					See local time
					</a></td>
				<td><%=flight.getArrivalDate() %></td>
				<td><%=flight.getArrivalTime() %></td>
			</tr>
		<% } %>
	</table>
</body>
</html>