<%-- 
    Document   : admin
    Created on : 12.11.2016, 16:19:20
    Author     : vaio
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
   String loginError = (String)session.getAttribute("login_error"); 
   session.removeAttribute("user");
   session.removeAttribute("login_error");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Login</title>
</head>
<body>
	<p class="error">
		<% if (loginError != null) {
			out.println(loginError);
		} %>
	</p>
	<div id="wrapper">
		<h1>Login</h1>
		<form id="loginForm" method="post" action="LoginServlet">
			<label for="username">Username:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> 
			<input type="text" id="username" name="username" required/><br><br>
			<label for="password">Password: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</label> 
			<input type="password" id="password" name="password" required/>
			<br><br>
			<div id="buttons">
				<input type="submit" value="Login" class="button">
				<input type="reset" value="Reset" class="button">
			</div>
		</form>
	</div>
</body>
</html>