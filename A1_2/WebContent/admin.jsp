<%-- 
    Document   : admin
    Created on : 12.11.2016, 16:17:58
    Author     : vaio
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="entities.Users"%>
<%@ page import="entities.Flights"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="entities.Cities"%>
<% Users user = (Users)session.getAttribute("user"); 
	if (user == null) {
		System.out.println("User is null...");
		response.sendRedirect("login.jsp");
		return;
	}

   List<Flights> allFlights = (List)session.getAttribute("allFlights");
   List<Users> allClients = (List)session.getAttribute("allClients");
   List<Cities> allCities = (List)session.getAttribute("allCities");
   
   String formMessage = (String)session.getAttribute("form_message"); 
   session.removeAttribute("form_message");
  
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="admin.js"></script>
	<style>
		input[type=checkbox] {
		    margin: 0 auto;
		    display: block;
		}
		#flights input[type="text"] {
			width: 120px;
		}
		.readonly, input[readonly] {
			background-color: rgb(235, 235, 228);
		}
		#logout {
			float: right;
		}
                
                body{
                    background-color: rgb(220,220,255);                    
                    
                }
	</style>
	<title>Admin panel</title>
</head>
<body>
	<p>
		<strong>
			<% if (formMessage != null) {
				out.println(formMessage);
			} %>
		</strong>
	</p>
	<% if (user.isIsAdmin()) { %>
		<a id="logout" href="login.jsp">LOGOUT</a>
		<p>Welcome, <%=user.getUsername() %>!</p>
		<h2>Insert flight:</h2>
		<form id="addFlight" method="post" action="AdminServlet">
			<input type="hidden" name="form_type" value="insert"/>
			<table>
				<tr>
					<td colspan="2">
						<label for="client">Client</label>
						<select name="client" required>
							<% for (Users client : allClients) { %>
								<option value="<%=client.getUsername() %>"><%=client.getUsername() %></option>
							<% } %>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="airplane_type">Airplane type</label>
						<input type="text" name="airplane_type" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="departure_city">Departure City</label>
						<select name="departure_city" id="departure_city" required>
							<% for (Cities city : allCities) { %>
								<option value="<%=city.getName() %>,<%=city.getLat() %>,<%=city.getLong_()%>"><%=city.getName() %></option>
							<% } %>
						</select>				
					</td>
					<td>
						<label for="arrival_city">Arrival City</label>
						<select name="arrival_city" id="arrival_city" required>
							<% for (Cities city : allCities) { %>
								<option value="<%=city.getName() %>,<%=city.getLat() %>,<%=city.getLong_()%>"><%=city.getName() %></option>
							<% } %>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="departure_date">Departure date</label>
						<input type="date" name="departure_date" required/>
					</td>
					<td>
						<label for="arrival_date">Arrival date</label>
						<input type="date" name="arrival_date" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="departure_time">Departure time</label>
						<input type="time" name="departure_time" required/>
					</td>
					<td>
						<label for="arrival_time">Arrival time</label>
						<input type="time" name="arrival_time" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Insert" class="button">
					<input type="reset" value="Reset" class="button"></td>
				</tr>
			</table>
		</form>
		
		<h2>All flights:</h2>
				<table border="1" id="flights">
					<tr>
						<th><strong>Update</strong></th>
						<th>Client</th>
						<th>Flight number</th>
						<th>Airplane type</th>
						<th>Departure city</th>
						<th>Departure date</th>
						<th>Departure time</th>
						<th>Arrival city</th>
						<th>Arrival date</th>
						<th>Arrival time</th>
						<th></th>
						<th></th>
					</tr>
					<% for (Flights flight : allFlights) { %>
						<tr class="flightRow" id="flight_<%=flight.getFlightNumber() %>">
							<form id="update_flight_<%=flight.getFlightNumber() %>"  method="post" action="AdminServlet">
							<input type="hidden" name="form_type" value="update"/>
							<td><input type="checkbox" id="<%=flight.getFlightNumber() %>" value="edit"/></td>
							<td><input type="text" name="client" value="<%=flight.getClientUsername() %>" readonly/></td>
							<td><input type="text" name="flight_number" value="<%=flight.getFlightNumber()%>" readonly /></td>
							<td><input type="text" name="airplane_type" class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getAirplaneType() %>" readonly /></td>
							<td class="readonly"><%=flight.getDepartureCity() %><input type="hidden" name="departure_city"  value="<%=flight.getDepartureCity() %>,<%=flight.getDepartureLatitude() %>,<%=flight.getDepartureLongitude() %>"  /></td>
							<td><input type="date" name="departure_date"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getDepartureDate() %>" readonly/></td>
							<td><input type="time" name="departure_time"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getDepartureTime() %>" readonly/></td>
							<td class="readonly"><%=flight.getArrivalCity() %><input type="hidden" name="arrival_city" value="<%=flight.getArrivalCity() %>,<%=flight.getArrivalLatitude() %>,<%=flight.getArrivalLongiture() %>"  /></td>
							<td><input type="date" name="arrival_date"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getArrivalDate() %>" readonly/></td>
							<td><input type="time" name="arrival_time"  class="update_<%=flight.getFlightNumber() %>" value="<%=flight.getArrivalTime() %>" readonly/></td>					
							<td><input class="update_btn_<%=flight.getFlightNumber() %>" type="submit" value="Update" disabled/></td>
							</form>
							<td>
								<form id="deleteFlight" method="post" action="AdminServlet">
									<input type="hidden" name="form_type" value="delete"/>
									<input type="hidden" name="del_flight_number" id="delFlightNumber" value="<%=flight.getFlightNumber() %>"/>
									<input type="submit" value="Delete" />
								</form>
							</td>
						</tr>
					<% } %>
				</table>
	<% } else { %>
		<p>You do not have access to this page, because you do not have admin rights.<br>
		<a href="login.jsp">Back to login.</a></p>
	<% } %>
</body>
</html>