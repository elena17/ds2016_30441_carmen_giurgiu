package entities;


import java.util.Date;

/**
 * Flights generated by hbm2java
 */
public class Flights implements java.io.Serializable {

	private Integer flightNumber;
	private String clientUsername;
	private String airplaneType;
	private String departureCity;
	private String departureLatitude;
	private String departureLongitude;
	private Date departureDate;
	private Date departureTime;
	private String arrivalCity;
	private String arrivalLatitude;
	private String arrivalLongiture;
	private Date arrivalDate;
	private Date arrivalTime;

	public Flights() {
	}

	public Flights(String clientUsername, String airplaneType, String departureCity, String departureLatitude,
			String departureLongitude, Date departureDate, Date departureTime, String arrivalCity,
			String arrivalLatitude, String arrivalLongiture, Date arrivalDate, Date arrivalTime) {
		this.clientUsername = clientUsername;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.departureLatitude = departureLatitude;
		this.departureLongitude = departureLongitude;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.arrivalCity = arrivalCity;
		this.arrivalLatitude = arrivalLatitude;
		this.arrivalLongiture = arrivalLongiture;
		this.arrivalDate = arrivalDate;
		this.arrivalTime = arrivalTime;
	}

	public Integer getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(Integer flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getClientUsername() {
		return this.clientUsername;
	}

	public void setClientUsername(String clientUsername) {
		this.clientUsername = clientUsername;
	}

	public String getAirplaneType() {
		return this.airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public String getDepartureCity() {
		return this.departureCity;
	}

	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}

	public String getDepartureLatitude() {
		return this.departureLatitude;
	}

	public void setDepartureLatitude(String departureLatitude) {
		this.departureLatitude = departureLatitude;
	}

	public String getDepartureLongitude() {
		return this.departureLongitude;
	}

	public void setDepartureLongitude(String departureLongitude) {
		this.departureLongitude = departureLongitude;
	}

	public Date getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getDepartureTime() {
		return this.departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalCity() {
		return this.arrivalCity;
	}

	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public String getArrivalLatitude() {
		return this.arrivalLatitude;
	}

	public void setArrivalLatitude(String arrivalLatitude) {
		this.arrivalLatitude = arrivalLatitude;
	}

	public String getArrivalLongiture() {
		return this.arrivalLongiture;
	}

	public void setArrivalLongiture(String arrivalLongiture) {
		this.arrivalLongiture = arrivalLongiture;
	}

	public Date getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getArrivalTime() {
		return this.arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

}
