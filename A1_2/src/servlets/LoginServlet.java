package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import dao.CitiesDAO;
import dao.FlightsDAO;
import dao.UsersDAO;
import entities.Users;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		UsersDAO usersDAO = new UsersDAO(new Configuration().configure().buildSessionFactory());
		
		HttpSession session = request.getSession(false);
		
		Users user = usersDAO.getUser(username);
		
		if (user == null) {
			session.setAttribute("login_error", "User does not exist! Please try again.");
			response.sendRedirect("login.jsp");
		}
		else if (!password.equals(user.getPassword())) {
			session.setAttribute("login_error", "Wrong password! Please try again.");
			response.sendRedirect("login.jsp");
		} else {
			// Login succeeded
			session.setAttribute("user", user);
			
			if (user.isIsAdmin()) {
				FlightsDAO flightsDAO = new FlightsDAO(new Configuration().configure().buildSessionFactory());
				List allFlights = flightsDAO.getAllFlights();
				session.setAttribute("allFlights", allFlights);
				
				List allClients = usersDAO.getAllClients();
				session.setAttribute("allClients", allClients);
				
				CitiesDAO citiesDAO = new CitiesDAO(new Configuration().configure().buildSessionFactory());
				List allCities = citiesDAO.getAllCities();
				session.setAttribute("allCities", allCities);
				
				response.sendRedirect("admin.jsp");
			} else {
				FlightsDAO flightsDAO = new FlightsDAO(new Configuration().configure().buildSessionFactory());
				List clientFlights = flightsDAO.getClientFlights(username);
				session.setAttribute("clientFlights", clientFlights);
				
				response.sendRedirect("client.jsp");
			}
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 processRequest(request, response);
	}

}
