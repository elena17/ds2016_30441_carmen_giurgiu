
package dao;


import java.util.List;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;

import entities.Flights;
import entities.Users;


public class FlightsDAO {

	private static final Log log = LogFactory.getLog(UsersDAO.class);

	SessionFactory sessionFactory;
	
	public FlightsDAO(SessionFactory factory) {
		this.sessionFactory = factory;
	}
	
	public Flights addFlight(Flights flight) {
		
		int flightNumber = -1;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			flightNumber = (Integer) session.save(flight);
			flight.setFlightNumber(flightNumber);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flight;
	}
	
	public void updateFlight(Flights flight) {
		
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
	}
	
	public void deleteFlight(int flightNumber) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
	      try{
	          tx = session.beginTransaction();
	          Flights flight = (Flights)session.get(Flights.class, flightNumber); 
	          session.delete(flight); 
	          tx.commit();
	       }catch (HibernateException e) {
	          if (tx!=null) tx.rollback();
	          e.printStackTrace(); 
	       }finally {
	          session.close(); 
	       }
	}
	
	public List getClientFlights(String username) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List clientFlights = null;

		try {
			 tx = session.getTransaction();
			 tx.begin();
			 Query query = session.createQuery("FROM Flights WHERE client_username='"+username+"'");
			 clientFlights = query.list();
			 tx.commit();
		 } catch (Exception e) {
			 if (tx != null) {
				 tx.rollback();
			 }
			 e.printStackTrace();
		 } finally {
			 session.close();
		 }
		
		return clientFlights;
	}
	
	public List getAllFlights() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List clientFlights = null;

		try {
			 tx = session.getTransaction();
			 tx.begin();
			 Query query = session.createQuery("FROM Flights");
			 clientFlights = query.list();
			 tx.commit();
		 } catch (Exception e) {
			 if (tx != null) {
				 tx.rollback();
			 }
			 e.printStackTrace();
		 } finally {
			 session.close();
		 }
		
		return clientFlights;
	}

}
