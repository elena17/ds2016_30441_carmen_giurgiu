package dao;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.Users;


public class UsersDAO {

	private static final Log log = LogFactory.getLog(UsersDAO.class);

	SessionFactory sessionFactory;
	
	public UsersDAO(SessionFactory factory) {
		this.sessionFactory = factory;
	}

	public Users getUser(String username) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Users user = null;

		try {
			 tx = session.getTransaction();
			 tx.begin();
			 Query query = session.createQuery("FROM Users WHERE username='"+username+"'");
			 user = (Users)query.uniqueResult();
			 tx.commit();
		 } catch (Exception e) {
			 if (tx != null) {
				 tx.rollback();
			 }
			 e.printStackTrace();
		 } finally {
			 session.close();
		 }
		
		return user;
	}
	
	public List getAllClients() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List clients = null;

		try {
			 tx = session.getTransaction();
			 tx.begin();
			 Query query = session.createQuery("FROM Users WHERE is_admin = 0");
			 clients = query.list();
			 tx.commit();
		 } catch (Exception e) {
			 if (tx != null) {
				 tx.rollback();
			 }
			 e.printStackTrace();
		 } finally {
			 session.close();
		 }
		
		return clients;
	}
}
