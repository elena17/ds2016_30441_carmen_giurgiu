package dao;


import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;

public class CitiesDAO {

	private static final Log log = LogFactory.getLog(CitiesDAO.class);

	SessionFactory sessionFactory;
	
	public CitiesDAO(SessionFactory factory) {
		this.sessionFactory = factory;
	}
	
	public List getAllCities() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List cities = null;

		try {
			 tx = session.getTransaction();
			 tx.begin();
			 Query query = session.createQuery("FROM Cities");
			 cities = query.list();
			 tx.commit();
		 } catch (Exception e) {
			 if (tx != null) {
				 tx.rollback();
			 }
			 e.printStackTrace();
		 } finally {
			 session.close();
		 }
		
		return cities;
	}
}
