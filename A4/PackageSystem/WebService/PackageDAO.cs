﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace WebService
{
    public class PackageDAO
   {

        public PackageDAO(){ }

     public bool addPackage(Package package){

         string cs = "server=localhost;userid=root;password=;database=ass4";

         MySqlConnection conn = null;

         try
         {
             conn = new MySqlConnection(cs);
             conn.Open();

             MySqlCommand cmd = new MySqlCommand();
             cmd.Connection = conn;
             cmd.CommandText = "INSERT INTO `assign5`.`packages` (`name`, `content`, `destination`, `weight`, `price`, `status`, `history`) VALUES (@Name, @Content, @Destination, @Weight, @Price, @Status, @History)";
             cmd.Prepare();
             cmd.Parameters.AddWithValue("@name", package.Name);
             cmd.Parameters.AddWithValue("@content",package.Content);
             cmd.Parameters.AddWithValue("@destination",package.Destination);
             cmd.Parameters.AddWithValue("@weight",package.Weight);
             cmd.Parameters.AddWithValue("@price", package.Price);
             cmd.Parameters.AddWithValue("@status", package.Status);
             cmd.Parameters.AddWithValue("@history", package.History);
             cmd.ExecuteNonQuery();

         }
         catch (MySqlException ex)
         {
             Console.WriteLine("Error: {0}", ex.ToString());
             return false;
         }
         finally
         {
             if (conn != null)
             {
                 conn.Close();
             }

         }

         System.Diagnostics.Debug.WriteLine("Package Added");
         return true;
    }
    

    public bool removePackage(int id){

        string cs = "server=localhost;userid=root;password=;database=ass4";

        MySqlConnection conn = null;

        try
        {
            conn = new MySqlConnection(cs);
            conn.Open();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "DELETE FROM `assign5`.`packages` WHERE `id`='"+ id +"';";
            cmd.Prepare();    
            cmd.ExecuteNonQuery();

        }
        catch (MySqlException ex)
        {
            Console.WriteLine("Error: {0}", ex.ToString());
            return false;
        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
            }

        }

        System.Diagnostics.Debug.WriteLine("Package Deleted");
        return true;
    }

    public Package getPackage(int id)
    {

        Package package = new Package();
        string cs = "server=localhost;userid=root;password=;database=assign5";

        MySqlConnection conn = null;

        try
        {
            conn = new MySqlConnection(cs);
            conn.Open();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "Select * from packages where id="+id+";";
            cmd.Prepare();
            cmd.ExecuteNonQuery();

        }
        catch (MySqlException ex)
        {
            Console.WriteLine("Error: {0}", ex.ToString());
          
        }
        finally
        {
            if (conn != null)
            {   conn.Close(); }

        }

        System.Diagnostics.Debug.WriteLine("Package Added");

        return package;
    }

    public List<Package> getPackages()
    {
        List<Package> packages = new List<Package>();
        Package package = new Package();
        string cs = "server=localhost;userid=root;password=;database=assign5";

        MySqlConnection conn = null;

        try
        {
            conn = new MySqlConnection(cs);
            conn.Open();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "Select * from packages;";
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                package.Id = int.Parse(rdr["id"].ToString());
                package.Name = rdr["name"].ToString();
                package.Content = rdr["content"].ToString();
                package.Destination = rdr["destination"].ToString();
                package.Price = float.Parse(rdr["price"].ToString());
                package.Weight = float.Parse(rdr["weight"].ToString());
                package.Status = rdr["status"].ToString();
                package.History = rdr["history"].ToString();
                packages.Add(package);
            }
            rdr.Close();

        }
        catch (MySqlException ex)
        {
            Console.WriteLine("Error: {0}", ex.ToString());
        }
        finally
        {
            if (conn != null)
            {   conn.Close();   }
        }

        System.Diagnostics.Debug.WriteLine("Package Added");

        return packages;
    }
}

}