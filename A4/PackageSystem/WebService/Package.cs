﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService
{
    public class Package
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        String status;

        public String Status
        {
            get { return status; }
            set { status = value; }
        }
        String history;

        public String History
        {
            get { return history; }
            set { history = value; }
        }

        String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        String content;

        public String Content
        {
            get { return content; }
            set { content = value; }
        }
        String destination;

        public String Destination
        {
            get { return destination; }
            set { destination = value; }
        }
        float price;

        public float Price
        {
            get { return price; }
            set { price = value; }
        }
        float weight;

        public float Weight
        {
            get { return weight; }
            set { weight = value; }
        }


        public Package()
        {

        }


    }
}