﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebService
{
    
   
    
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        PackageDAO packageDAO = new PackageDAO();


         [WebMethod]
         public string addPackage(string name, string destination, string content, float price, float weight, string status, string history)
         {
             Package package = new Package();
             package.Name = name;
             package.Content = content;
             package.Destination = destination;
             package.Price = price;
             package.Weight = weight;
             package.Status = status;
             package.History = history;

             packageDAO.addPackage(package);

             return "true";
         }

        [WebMethod]
         public void removePackage(int id)
         {
             packageDAO.removePackage(id);
         }


        [WebMethod]
        public List<Package> getPackages()
        {
            return packageDAO.getPackages();

        }

     
    }
}
