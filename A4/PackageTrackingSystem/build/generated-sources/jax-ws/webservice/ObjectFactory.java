
package webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AssignPackage_QNAME = new QName("http://webservice/", "assignPackage");
    private final static QName _AssignPackageResponse_QNAME = new QName("http://webservice/", "assignPackageResponse");
    private final static QName _UpdatePackage_QNAME = new QName("http://webservice/", "updatePackage");
    private final static QName _GetPackagesForUserResponse_QNAME = new QName("http://webservice/", "getPackagesForUserResponse");
    private final static QName _UpdatePackageResponse_QNAME = new QName("http://webservice/", "updatePackageResponse");
    private final static QName _GetAllPackagesResponse_QNAME = new QName("http://webservice/", "getAllPackagesResponse");
    private final static QName _GetAllPackages_QNAME = new QName("http://webservice/", "getAllPackages");
    private final static QName _GetPackagesForUser_QNAME = new QName("http://webservice/", "getPackagesForUser");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AssignPackage }
     * 
     */
    public AssignPackage createAssignPackage() {
        return new AssignPackage();
    }

    /**
     * Create an instance of {@link AssignPackageResponse }
     * 
     */
    public AssignPackageResponse createAssignPackageResponse() {
        return new AssignPackageResponse();
    }

    /**
     * Create an instance of {@link GetPackagesForUserResponse }
     * 
     */
    public GetPackagesForUserResponse createGetPackagesForUserResponse() {
        return new GetPackagesForUserResponse();
    }

    /**
     * Create an instance of {@link UpdatePackageResponse }
     * 
     */
    public UpdatePackageResponse createUpdatePackageResponse() {
        return new UpdatePackageResponse();
    }

    /**
     * Create an instance of {@link UpdatePackage }
     * 
     */
    public UpdatePackage createUpdatePackage() {
        return new UpdatePackage();
    }

    /**
     * Create an instance of {@link GetAllPackagesResponse }
     * 
     */
    public GetAllPackagesResponse createGetAllPackagesResponse() {
        return new GetAllPackagesResponse();
    }

    /**
     * Create an instance of {@link GetAllPackages }
     * 
     */
    public GetAllPackages createGetAllPackages() {
        return new GetAllPackages();
    }

    /**
     * Create an instance of {@link GetPackagesForUser }
     * 
     */
    public GetPackagesForUser createGetPackagesForUser() {
        return new GetPackagesForUser();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "assignPackage")
    public JAXBElement<AssignPackage> createAssignPackage(AssignPackage value) {
        return new JAXBElement<AssignPackage>(_AssignPackage_QNAME, AssignPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "assignPackageResponse")
    public JAXBElement<AssignPackageResponse> createAssignPackageResponse(AssignPackageResponse value) {
        return new JAXBElement<AssignPackageResponse>(_AssignPackageResponse_QNAME, AssignPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "updatePackage")
    public JAXBElement<UpdatePackage> createUpdatePackage(UpdatePackage value) {
        return new JAXBElement<UpdatePackage>(_UpdatePackage_QNAME, UpdatePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPackagesForUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getPackagesForUserResponse")
    public JAXBElement<GetPackagesForUserResponse> createGetPackagesForUserResponse(GetPackagesForUserResponse value) {
        return new JAXBElement<GetPackagesForUserResponse>(_GetPackagesForUserResponse_QNAME, GetPackagesForUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "updatePackageResponse")
    public JAXBElement<UpdatePackageResponse> createUpdatePackageResponse(UpdatePackageResponse value) {
        return new JAXBElement<UpdatePackageResponse>(_UpdatePackageResponse_QNAME, UpdatePackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPackagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getAllPackagesResponse")
    public JAXBElement<GetAllPackagesResponse> createGetAllPackagesResponse(GetAllPackagesResponse value) {
        return new JAXBElement<GetAllPackagesResponse>(_GetAllPackagesResponse_QNAME, GetAllPackagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPackages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getAllPackages")
    public JAXBElement<GetAllPackages> createGetAllPackages(GetAllPackages value) {
        return new JAXBElement<GetAllPackages>(_GetAllPackages_QNAME, GetAllPackages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPackagesForUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice/", name = "getPackagesForUser")
    public JAXBElement<GetPackagesForUser> createGetPackagesForUser(GetPackagesForUser value) {
        return new JAXBElement<GetPackagesForUser>(_GetPackagesForUser_QNAME, GetPackagesForUser.class, null, value);
    }

}
