package table;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

public class Table extends AbstractTableModel {
	
	private static final long serialVersionUID = 1;
	
	String[] columnNames={};
    Vector lines=new Vector();

    public Table(Vector v)
    {
        this.lines=v;
        Vector row=(Vector)lines.elementAt(0);
        int nr=row.size();
        columnNames=new String[nr];
        for(int i=0;i<nr;i++)
        	columnNames[i]="Column "+i;
    }

    @Override
    public String getColumnName(int column)
    {
       return "Column "+column;
    }

    public int getColumnCount()
    {
        return columnNames.length;
    }

    public int getRowCount()
    {
        return lines.size();
    }

    public Object getValueAt(int line,int column)
    {
        Vector row=(Vector)lines.elementAt(line);
        return row.elementAt(column);
    }

}
