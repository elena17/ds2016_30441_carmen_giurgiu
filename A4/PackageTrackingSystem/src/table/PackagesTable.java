
package table;

import java.util.Vector;

public class PackagesTable extends Table{
    private static final long serialVersionUID = 1;
	
	public PackagesTable(Vector v)
    {
        super(v);
    }

    
    public String getColumnName(int column)
    {
       String n="";
       if(column==0) n="Package ID";
       if(column==1) n="Name";
       if(column==2) n="Status";
       if(column==3) n="History";
       if(column==4) n="Weight";
       if(column==5) n="Registered";
       return n;
    }
}
