package packagetrackingsystem;



import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import webservice.User;


public class RegisterWindow extends JFrame implements ActionListener{

	 public static final long serialVersionUID = 1;
	
	private JPanel contentPane;
	private JTextField usernameTextField;
	private JTextField passwordTextField;
	JButton btnLogin = new JButton("Register");
	
	

	/**
	 * Create the frame.
	 */
	public RegisterWindow() {
		
		
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(471,232);
		setLocation(420,120);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcome = new JLabel("Welcome!");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Forte", Font.PLAIN, 26));
		lblWelcome.setBounds(132, 11, 178, 53);
		contentPane.add(lblWelcome);
		
		JLabel lblNewLabel = new JLabel("Username: ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Forte", Font.PLAIN, 18));
		lblNewLabel.setBounds(81, 75, 88, 28);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password: ");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setFont(new Font("Forte", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(81, 98, 88, 34);
		contentPane.add(lblNewLabel_1);
		
		usernameTextField = new JTextField();
		usernameTextField.setText("");
		usernameTextField.setFont(new Font("Forte", Font.PLAIN, 14));
		usernameTextField.setBounds(179, 81, 178, 20);
		contentPane.add(usernameTextField);
		usernameTextField.setColumns(10);
		
		passwordTextField = new JPasswordField();
		passwordTextField.setText("");
		passwordTextField.setFont(new Font("Forte", Font.PLAIN, 14));
		passwordTextField.setBounds(179, 107, 178, 20);
		contentPane.add(passwordTextField);
		passwordTextField.setColumns(10);
		
		
		btnLogin.setBounds(189, 149, 100, 23);
		btnLogin.setFont(new Font("Forte", Font.PLAIN, 18));
		btnLogin.addActionListener(this);
		contentPane.add(btnLogin);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		
		
		if (e.getActionCommand().compareTo("Register")==0)
		{
			
			if (usernameTextField.getText().isEmpty() || passwordTextField.getText().isEmpty()) {
				System.out.println("Client side error ...Missing input!");
				JOptionPane.showMessageDialog(null,"Missing input!");
			} else {
			
			String username = usernameTextField.getText();
			String password = passwordTextField.getText();
			
                        int res = registerUser(username, password);
                        if (res == 1) {
                            LoginWindow win1=new LoginWindow();
                        } else {
                            JOptionPane.showMessageDialog(null,"Error!");
                        }
				
			}
		}
	}

    private static int registerUser(java.lang.String username, java.lang.String password) {
        webservice.UserWS_Service service = new webservice.UserWS_Service();
        webservice.UserWS port = service.getUserWSPort();
        return port.registerUser(username, password);
    }

   
        
        
        
}
