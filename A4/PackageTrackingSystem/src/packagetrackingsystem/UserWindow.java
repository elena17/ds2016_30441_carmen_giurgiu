

package packagetrackingsystem;
import forms.*;
import javax.swing.JFrame;
import webservice.User;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import table.PackagesTable;
import table.Table;

import webservice.Package;

public class UserWindow extends JFrame{
    
    private User user;
    ArrayList <webservice.Package> winPack;
    public static final long serialVersionUID = 1;
    
     public UserWindow(User u){
        super("User");
        winPack = new ArrayList<webservice.Package>();
        this.user = u;
        
        Toolkit t=this.getToolkit();
		Dimension marimeEcran=Toolkit.getDefaultToolkit().getScreenSize();
		int pozitie=200;
		setBounds(pozitie,pozitie,marimeEcran.width-2*pozitie,marimeEcran.height/2-30);


		createTopBars();
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				e.getWindow().dispose();
				System.exit(0);
			}
		});

		this.setVisible(true);
                setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
     }
    
     
      private void createTopBars()
	{
		JMenuBar menuBar = new JMenuBar();
		menuBar.setOpaque(true);

		JMenu menuSearch=new JMenu("Packages");
		JMenuItem search0=new JMenuItem("My packages");
		search0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				System.out.println("My packages");
				listAllPackages();
			}
		});
		

		menuSearch.add(search0);
		menuBar.add(menuSearch);		
		setJMenuBar(menuBar);
	}
      
      public void listAllPackages(){
           ArrayList <webservice.Package> p = new ArrayList<webservice.Package>();
        p = (ArrayList<Package>) getPackagesForUser(user.getUserId());
        winPack = p;
        Vector pack = this.makeTable(p);
        if(pack.size()>0)
        {
                this.getContentPane().removeAll();
                Table t=new PackagesTable(pack);
                this.getContentPane().add(new JScrollPane(new JTable(t)),BorderLayout.NORTH);
                this.invalidate();
                this.validate();               
        }
        else
        {
                JOptionPane.showMessageDialog(this,"There are no packages to display!","",1);
        }
      }
      
      
       private Vector makeTable(ArrayList<webservice.Package> packages)
	{
		Vector v=new Vector();

		int nr=packages.size();
		for(int i=0;i<nr;i++)
		{
			System.out.println("((((");
			Vector row=new Vector();
			Package p=packages.get(i);
			row.add(p.getId());
			row.add(p.getName());
			row.add(p.getStatus());
                        row.add(p.getHistory());
			row.add(p.getWeight());
                        if (p.isIsTaken()){
                            row.add("YES");
                        } else {
                            row.add("NO");
                        }
                        
//			row.add(job.getContact());
//			row.add(job.getSpecification());
			v.add(row);
		}
		this.validate();
		return v;
	}

    private static java.util.List<webservice.Package> getPackagesForUser(int userId) {
        webservice.PackageWS_Service service = new webservice.PackageWS_Service();
        webservice.PackageWS port = service.getPackageWSPort();
        return port.getPackagesForUser(userId);
    }
}
