package packagetrackingsystem;



import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import webservice.User;


public class MainWindow extends JFrame implements ActionListener{

	 public static final long serialVersionUID = 1;
	
	private JPanel contentPane;
	
	JButton btnLogin = new JButton("Login");
        JButton btnRegister = new JButton("Register");
	
	

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		
		
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(471,232);
		setLocation(420,120);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcome = new JLabel("Welcome!");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Forte", Font.PLAIN, 26));
		lblWelcome.setBounds(155, 11, 178, 53);
		contentPane.add(lblWelcome);
		
		
		btnRegister.setBounds(189, 70, 100, 23);
		btnRegister.setFont(new Font("Forte", Font.PLAIN, 18));
		btnRegister.addActionListener(this);
		
		
		
		btnLogin.setBounds(189, 110, 100, 23);
		btnLogin.setFont(new Font("Forte", Font.PLAIN, 18));
		btnLogin.addActionListener(this);
		contentPane.add(btnLogin);
                contentPane.add(btnRegister);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		
		
		if (e.getActionCommand().compareTo("Register")==0)
		{
                        RegisterWindow win1=new RegisterWindow();
		} else if (e.getActionCommand().compareTo("Login")==0) {
                    LoginWindow win1=new LoginWindow();
                }
	}
   
}
