
package packagetrackingsystem;

import forms.*;
import javax.swing.JFrame;
import webservice.User;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import table.PackagesTable;
import table.Table;

import webservice.Package;


public class AdminWindow extends JFrame {
    
    private User user;
    ArrayList <webservice.Package> winPack;
    public static final long serialVersionUID = 1;
    
    public AdminWindow(User u){
        super("Admin");
        winPack = new ArrayList<webservice.Package>();
        this.user = u;
        Toolkit t=this.getToolkit();
		Dimension marimeEcran=Toolkit.getDefaultToolkit().getScreenSize();
		int pozitie=200;
		setBounds(pozitie,pozitie,marimeEcran.width-2*pozitie,marimeEcran.height/2-30);


		createTopBars();
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				e.getWindow().dispose();
				System.exit(0);
			}
		});

		this.setVisible(true);
                setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    
    
    
    private void createTopBars()
	{
		JMenuBar menuBar = new JMenuBar();
		menuBar.setOpaque(true);

		JMenu menuSearch=new JMenu("Packages");
		JMenuItem search0=new JMenuItem("List all packages");
		search0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				System.out.println("List all jobs");
				listAllPackages();
			}
		});
		JMenuItem search1=new JMenuItem("Add package");
		search1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				System.out.println("Add package");
				addPackage();
			}
		});
		JMenuItem search2=new JMenuItem("Remove package");
		search2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				System.out.println("Remove package");
				removePackage();
			}
		});
		JMenuItem search3=new JMenuItem("Register package");
		search3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				System.out.println("Register package");
				registerPackage();
			}
		});
		
                JMenuItem search4=new JMenuItem("Update package");
		search4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				System.out.println("Update package");
				editPackage();
			}
		});

		menuSearch.add(search0);
		menuSearch.addSeparator();
		menuSearch.add(search1);
		menuSearch.addSeparator();
		menuSearch.add(search2);
		menuSearch.addSeparator();
		menuSearch.add(search3);
		menuSearch.addSeparator();
                menuSearch.add(search4);
		menuBar.add(menuSearch);		
		setJMenuBar(menuBar);
	}
    
    
    public void listAllPackages(){
        ArrayList <webservice.Package> p = new ArrayList<webservice.Package>();
        p = (ArrayList<Package>) getAllPackages();
        winPack = p;
        Vector pack = this.makeTable(p);
        if(pack.size()>0)
        {
                this.getContentPane().removeAll();
                Table t=new PackagesTable(pack);
                this.getContentPane().add(new JScrollPane(new JTable(t)),BorderLayout.NORTH);
                this.invalidate();
                this.validate();               
        }
        else
        {
                JOptionPane.showMessageDialog(this,"There are no packages to display!","",1);
        }
    }
    
    public void addPackage() {
        AddPackageForm f=new AddPackageForm(this,"Add package",true);
        boolean cancel=f.isCanceled();
        if (cancel == false) {
            if (f.getStatus().equals("") || f.getName().equals("") || f.getWeight().equals(""))
                JOptionPane.showMessageDialog(this,"You must complete all fields!","",1);
            else {
              String name = f.getName();
              String status = f.getStatus();
              int w = Integer.parseInt(f.getWeight());
               boolean x = false;
               x= insertPackage(name, status, w);
               if (x){
                   ArrayList <webservice.Package> p = new ArrayList<webservice.Package>();
                    p = (ArrayList<Package>) getAllPackages();
                    winPack = p;
                    Vector pack = this.makeTable(p);
                if(pack.size()>0)
                {
                        this.getContentPane().removeAll();
                        Table t=new PackagesTable(pack);
                        this.getContentPane().add(new JScrollPane(new JTable(t)),BorderLayout.NORTH);
                        this.invalidate();
                        this.validate(); 
                        JOptionPane.showMessageDialog(this,"Package added!","",1);
                }  
               } else {
                  JOptionPane.showMessageDialog(this,"Error!","",1); 
               }
            }
        }
    }
    
    public void removePackage() 
    {
         RemovePackageForm f = new RemovePackageForm(this, "Remove Package", true, winPack);
          boolean cancel=f.isCanceled();
        if (cancel == false) {
            int idDel = Integer.parseInt(f.getPackage());
            System.out.println(idDel);
            boolean x = deletePackage(idDel);
            if (x){
                ArrayList <webservice.Package> p2 = new ArrayList<webservice.Package>();
                    p2 = (ArrayList<Package>) getAllPackages();
                    winPack = p2;
                    Vector pack = this.makeTable(p2);
                if(pack.size()>0)
                {
                        this.getContentPane().removeAll();
                        Table t=new PackagesTable(pack);
                        this.getContentPane().add(new JScrollPane(new JTable(t)),BorderLayout.NORTH);
                        this.invalidate();
                        this.validate(); 
                        JOptionPane.showMessageDialog(this,"Package removed!","",1);
                }  
            } else {
                JOptionPane.showMessageDialog(this,"Error!","",1);
            }
        }
    }
    
     public void registerPackage() 
    {
         ArrayList <webservice.User> users = new ArrayList<webservice.User>();
         users = (ArrayList<User>) getAllUsers();
         ArrayList <webservice.Package> packs = new ArrayList<webservice.Package>();
         int nr=this.winPack.size();
         for(int i=0;i<nr;i++)
	 {
               webservice.Package y = this.winPack.get(i);
               if (!y.isIsTaken()){
                   packs.add(y);
               }
         }
         
         if (packs.size() != 0) {
             RegisterPackageForm f = new RegisterPackageForm(this, "Register Package", true, packs, users);
          boolean cancel=f.isCanceled();
         if (cancel == false) {
             if (f.getPackage().equals("") || f.getUser().equals("")) {
                  JOptionPane.showMessageDialog(this,"You must complete all fields!","",1);
             } else {
              int idP = Integer.parseInt(f.getPackage());
             System.out.println(idP);
             int idU = Integer.parseInt(f.getUser());
             System.out.println(idU);
             
             int x = assignPackage(idU, idP);
                 updatePackage(idP, "registered");
                ArrayList <webservice.Package> p2 = new ArrayList<webservice.Package>();
                    p2 = (ArrayList<Package>) getAllPackages();
                    winPack = p2;
                    Vector pack = this.makeTable(p2);
                if(pack.size()>0)
                {
                        this.getContentPane().removeAll();
                        Table t=new PackagesTable(pack);
                        this.getContentPane().add(new JScrollPane(new JTable(t)),BorderLayout.NORTH);
                        this.invalidate();
                        this.validate(); 
                        JOptionPane.showMessageDialog(this,"Package registered!","",1);
                } 
             
             
             }
         }
         } else {
             JOptionPane.showMessageDialog(this,"All packages are taken!","",1);
             
         }
         
         
    }
     
     
     public void editPackage(){
         UpdatePackageForm f = new UpdatePackageForm(this, "Update package", true, winPack);
         boolean cancel=f.isCanceled();
         if (cancel == false) {
             if (f.getStatus().equals("") || f.getPackage().equals("")){
                 JOptionPane.showMessageDialog(this,"You must complete all fields!","",1);
             } else {
                 int idP = Integer.parseInt(f.getPackage());
                    System.out.println(idP);
                    String newStatus = f.getStatus();
                    int x = updatePackage(idP, newStatus);
                    ArrayList <webservice.Package> p2 = new ArrayList<webservice.Package>();
                    p2 = (ArrayList<Package>) getAllPackages();
                    winPack = p2;
                    Vector pack = this.makeTable(p2);
                if(pack.size()>0)
                {
                        this.getContentPane().removeAll();
                        Table t=new PackagesTable(pack);
                        this.getContentPane().add(new JScrollPane(new JTable(t)),BorderLayout.NORTH);
                        this.invalidate();
                        this.validate(); 
                        JOptionPane.showMessageDialog(this,"Package updated!","",1);
                } 
             }
         }
     }
     
     private Vector makeTable(ArrayList<webservice.Package> packages)
	{
		Vector v=new Vector();

		int nr=packages.size();
		for(int i=0;i<nr;i++)
		{
			System.out.println("((((");
			Vector row=new Vector();
			Package p=packages.get(i);
			row.add(p.getId());
			row.add(p.getName());
			row.add(p.getStatus());
                        row.add(p.getHistory());
			row.add(p.getWeight());
                        if (p.isIsTaken()){
                            row.add("YES");
                        } else {
                            row.add("NO");
                        }
                        
//			row.add(job.getContact());
//			row.add(job.getSpecification());
			v.add(row);
		}
		this.validate();
		return v;
	}

    private static boolean insertPackage(java.lang.String name, java.lang.String status, int weight) {
        org.tempuri.WebService1 service = new org.tempuri.WebService1();
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.insertPackage(name, status, weight);
    }

    private static java.util.List<webservice.Package> getAllPackages() {
        webservice.PackageWS_Service service = new webservice.PackageWS_Service();
        webservice.PackageWS port = service.getPackageWSPort();
        return port.getAllPackages();
    }

    private static boolean deletePackage(int id) {
        org.tempuri.WebService1 service = new org.tempuri.WebService1();
        org.tempuri.WebService1Soap port = service.getWebService1Soap();
        return port.deletePackage(id);
    }

    private static java.util.List<webservice.User> getAllUsers() {
        webservice.UserWS_Service service = new webservice.UserWS_Service();
        webservice.UserWS port = service.getUserWSPort();
        return port.getAllUsers();
    }

    private static int assignPackage(int userId, int packageId) {
        webservice.PackageWS_Service service = new webservice.PackageWS_Service();
        webservice.PackageWS port = service.getPackageWSPort();
        return port.assignPackage(userId, packageId);
    }

    private static int updatePackage(int packageId, java.lang.String status) {
        webservice.PackageWS_Service service = new webservice.PackageWS_Service();
        webservice.PackageWS port = service.getPackageWSPort();
        return port.updatePackage(packageId, status);
    }

   
    
}
