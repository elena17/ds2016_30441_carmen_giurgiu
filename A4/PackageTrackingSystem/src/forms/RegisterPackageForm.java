
package forms;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class RegisterPackageForm extends AbstractForm{
    
    private JLabel label;
    private JLabel label2;
	 private JComboBox t;
         private JComboBox t2;
          private ArrayList<webservice.Package> packsAvailable;
           private ArrayList<webservice.User> usersAvailable;
          
          public RegisterPackageForm(JFrame f,String title,boolean modal,ArrayList<webservice.Package> pack, ArrayList<webservice.User> users)
    {
        super(f,title,modal);
        this.usersAvailable = users;
        this.packsAvailable = pack;
        init();
        Dimension marimeEcran=Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(marimeEcran.width/2-150,marimeEcran.height/2-100,250,160);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cancel=true;
                e.getWindow().dispose();
            }
        });
        setVisible(true);
    }
          
          @Override
	protected void init() {
		contentPane=new JPanel();
    	contentPane.setLayout(null);
    	
            this.label=new JLabel("Choose package:");
	    this.label.setBounds(15,20,130,30);
            this.label2=new JLabel("Choose user:");
	    this.label2.setBounds(15,50,130,30);

	       int nr=this.packsAvailable.size();
	       String jobs[] = new String[nr];
	       for(int i=0;i<nr;i++)
	       {
                   String ids = Integer.toString(this.packsAvailable.get(i).getId());
                   
	    	   jobs[i] = ids;
	          // t.addItem(jobsAvailable.get(i).getTitle());
	       }
	       
	       t=new JComboBox(jobs);
	       t.setBounds(120,25,100,20);
               
               int nr2=this.usersAvailable.size();
	       String users[] = new String[nr2];
	       for(int i=0;i<nr2;i++)
	       {
                   String ids = Integer.toString(this.usersAvailable.get(i).getUserId());
	    	   users[i] = ids;
	          // t.addItem(jobsAvailable.get(i).getTitle());
	       }
               t2=new JComboBox(users);
	       t2.setBounds(120,55,100,20);
               
	       buton=new JButton("OK");
	       buton.setBounds(80,85,70,30);
	       buton.addActionListener(new ActionListener() {
	           public void actionPerformed(ActionEvent e){
	               buttonPressed();
	           }
	       });
	       contentPane.add(label2,null);
               contentPane.add(t2,null);
	       contentPane.add(t,null);
	       contentPane.add(label,null);
	       contentPane.add(buton,null);

	       this.getContentPane().add(contentPane,null);
		
	}

	@Override
	protected void buttonPressed() {
		this.dispose();
		
	}

	@Override
	public boolean isCanceled() {
		return this.cancel;
	}
	
	public String getPackage() {
		
		return (String)this.t.getSelectedItem();
	}
        
        public String getUser() {
            return (String)this.t2.getSelectedItem();
        }
    
}
