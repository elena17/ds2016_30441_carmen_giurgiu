

package forms;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import webservice.Package;


public class RemovePackageForm extends AbstractForm{
        private JLabel label;
	 private JComboBox t;
          private ArrayList<webservice.Package> packsAvailable;
          
          public RemovePackageForm(JFrame f,String title,boolean modal,ArrayList<webservice.Package> pack)
    {
        super(f,title,modal);
        this.packsAvailable = pack;
        init();
        Dimension marimeEcran=Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(marimeEcran.width/2-150,marimeEcran.height/2-100,250,150);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cancel=true;
                e.getWindow().dispose();
            }
        });
        setVisible(true);
    }
          
          @Override
	protected void init() {
		contentPane=new JPanel();
    	contentPane.setLayout(null);
    	
    	this.label=new JLabel("Choose package:");
	    this.label.setBounds(15,20,130,30);

	       int nr=this.packsAvailable.size();
	       String jobs[] = new String[nr];
	       for(int i=0;i<nr;i++)
	       {
                   String ids = Integer.toString(this.packsAvailable.get(i).getId());
	    	   jobs[i] = ids;
	          // t.addItem(jobsAvailable.get(i).getTitle());
	       }
	       
	       t=new JComboBox(jobs);
	       t.setBounds(120,25,100,20);
	       buton=new JButton("OK");
	       buton.setBounds(80,60,70,30);
	       buton.addActionListener(new ActionListener() {
	           public void actionPerformed(ActionEvent e){
	               buttonPressed();
	           }
	       });
	       
	       contentPane.add(t,null);
	       contentPane.add(label,null);
	       contentPane.add(buton,null);

	       this.getContentPane().add(contentPane,null);
		
	}

	@Override
	protected void buttonPressed() {
		this.dispose();
		
	}

	@Override
	public boolean isCanceled() {
		return this.cancel;
	}
	
	public String getPackage() {
		
		return (String)this.t.getSelectedItem();
	}
}
