package forms;
import javax.swing.*;

abstract class AbstractForm extends JDialog {
	
	 JPanel contentPane;
     JButton buton;
     boolean cancel=false;

     public AbstractForm(JFrame f,String title,boolean modal)
     {
         super(f,title,modal);
     }

     protected abstract void init();
     protected abstract void buttonPressed();
     protected abstract boolean isCanceled();

}
