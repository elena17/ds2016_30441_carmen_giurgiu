
package forms;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class UpdatePackageForm extends AbstractForm{
            private JLabel label1,label2,label3,label4,label5,label6;
	    private JTextField text1,text2,text3,text4;
	    private JComboBox t;
	    private ArrayList<webservice.Package> packsAvailable;
	   

	    public UpdatePackageForm(JFrame f,String title,boolean modal,ArrayList<webservice.Package> pack)
	    {
	        super(f,title,modal);
	        this.packsAvailable = pack;
	        init();
	        Dimension marimeEcran=Toolkit.getDefaultToolkit().getScreenSize();
	        setBounds(marimeEcran.width/2-150,marimeEcran.height/2-100,320,300);
	        addWindowListener(new WindowAdapter() {
	            @Override
	            public void windowClosing(WindowEvent e) {
	                cancel=true;
	                e.getWindow().dispose();
	            }
	        });
	        setVisible(true);
	    }

	    @Override
	    protected void init()
	    {
	    	contentPane=new JPanel();
	    	contentPane.setLayout(null);

	       label1=new JLabel("Package:");
	       label1.setBounds(15,15,130,30);
	        int nr=this.packsAvailable.size();
	       String jobs[] = new String[nr];
	       for(int i=0;i<nr;i++)
	       {
                   String ids = Integer.toString(this.packsAvailable.get(i).getId());
                   
	    	   jobs[i] = ids;
	          // t.addItem(jobsAvailable.get(i).getTitle());
	       }
	       
	       t=new JComboBox(jobs);
	       t.setBounds(120,20,100,20);

	       label2=new JLabel("Status:");
	       label2.setBounds(15,70,130,30);
	       text2=new JTextField();
	       text2.setBounds(120,75,150,20);

	 
	       

	       buton=new JButton("Add");
	       buton.setBounds(100,220,70,30);
	       buton.addActionListener(new ActionListener() {
	           public void actionPerformed(ActionEvent e){
	               buttonPressed();
	           }
	       });

	       contentPane.add(label1,null);
	       contentPane.add(label2,null);
	       
	    
	  
	       contentPane.add(t,null);
	       contentPane.add(text2,null);
	      
	       
	     
	     
	       contentPane.add(buton,null);

	       this.getContentPane().add(contentPane,null);
	    }

	    @Override
	    protected void buttonPressed()
	    {
	        this.dispose();
	    }

	    
	   

	    public String getStatus()
	    {
	        return text2.getText();
	    }

	    public String getPackage() {
		
		return (String)this.t.getSelectedItem();
	}

	   

	   

	    @Override
	    public boolean isCanceled()
	    {
	        return this.cancel;
	    }

	}
