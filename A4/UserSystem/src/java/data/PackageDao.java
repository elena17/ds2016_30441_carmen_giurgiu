package data;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class PackageDao {
    
     private static Connection conn=null;
	private static String driver="com.mysql.jdbc.Driver";
	private static String dbName="ass4",
							username="root",
							password="";
	
	  public static PackageDao instance;
	  
	  private PackageDao(){	  }
	  
	  public static PackageDao getInstance(){
	  
	  	if(instance==null){  	
	  		instance = new PackageDao();
	  	} 
	  return instance;
	  
	  }
	
	/**
	 * ESTABLISH CONNECTION
	 * @return
	 */
	public static Connection connect(){
		
		try{
		Class.forName(driver);
		conn= (Connection) DriverManager.getConnection("jdbc:mysql://"+"localhost:3306/ass4",username,password);
	
		}catch(Exception e){
				System.out.println("Connection Error");
			}
		
		return conn;
	}
	
    public ResultSet getAllPackages(Connection connection) {
	
	ResultSet resultSet = null;
	
	
	try {
		Statement statement = (Statement) connection.createStatement(
				ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY,
				ResultSet.HOLD_CURSORS_OVER_COMMIT);

		

		String selectAll = "Select * from packages";

		resultSet = statement.executeQuery(selectAll);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
	
	return resultSet;
}
    public ResultSet getPackageById(Connection connection,int id) {
	
	ResultSet resultSet = null;
	
	
	try {
		Statement statement = (Statement) connection.createStatement(
				ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY,
				ResultSet.HOLD_CURSORS_OVER_COMMIT);

		

		String selectAll = "Select * from packages where id="+id;

		resultSet = statement.executeQuery(selectAll);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
	
	return resultSet;
}
    
    
     public int insertUserPackage(Connection connection,int userId, int packageId) throws SQLException {
	Statement st = (Statement) this.conn.createStatement();
	st.execute("insert into users_packages(userId, packageId)" +
			" values ("+ userId+"," + packageId +")", Statement.RETURN_GENERATED_KEYS);
	ResultSet generated = st.getGeneratedKeys();
	if (generated.next()) {
		return 1;
	}
	return -1;
    }
     
     public void updatePackageTaken(Connection connection, int pId) {
         try {
			// connection.setAutoCommit(false);
			java.sql.Statement statement = conn.createStatement();
			statement
					.executeUpdate("update packages set packages.isTaken=1 where packages.id="
							+ pId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
     }
     
     public void updatePackageStatus(Connection connection, int id, String stat) {
         try {
			// connection.setAutoCommit(false);
			java.sql.Statement statement = conn.createStatement();
			statement
					.executeUpdate("update packages set packages.status='"+stat+"',packages.history=CONCAT(packages.history,'; "+stat+"')  where packages.id="
							+ id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
     }
     
     
     public ResultSet getPackagesByUserId(Connection connection,int id) {
	
	ResultSet resultSet = null;
	
	
	try {
		Statement statement = (Statement) connection.createStatement(
				ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY,
				ResultSet.HOLD_CURSORS_OVER_COMMIT);

		

		String selectAll = "Select * from users_packages where userId="+id;

		resultSet = statement.executeQuery(selectAll);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
	
	return resultSet;
}
}
