
package data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnection {
    public Connection con;
	 
	 public DBConnection() {
	  setConnection();
	 }
	 
	 
	 private void setConnection() {
	  try {
	   Class.forName("com.mysql.jdbc.Driver");
	   con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/asg4","root", "pass");
	  } catch (SQLException e) {
	   e.printStackTrace();
	  } catch (ClassNotFoundException e) {
	   e.printStackTrace();
	  } 
	 }
	 
	 public Connection getConnection() {
	  return con;
	 }
}
