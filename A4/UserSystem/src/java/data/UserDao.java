package data;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class UserDao {
    
    private static Connection conn=null;
	private static String driver="com.mysql.jdbc.Driver";
	private static String dbName="ass4",
							username="root",
							password="";
	
	  public static UserDao instance;
	  
	  private UserDao(){	  }
	  
	  public static UserDao getInstance(){
	  
	  	if(instance==null){  	
	  		instance = new UserDao();
	  	} 
	  return instance;
	  
	  }
	
	/**
	 * ESTABLISH CONNECTION
	 * @return
	 */
	public static Connection connect(){
		
		try{
		Class.forName(driver);
		conn= (Connection) DriverManager.getConnection("jdbc:mysql://"+"localhost:3306/ass4",username,password);
	
		}catch(Exception e){
				System.out.println("Connection Error");
			}
		
		return conn;
	}
	
	
	

	
	
	public ResultSet getUser(Connection connection,String userName,String password) {

		String query = "Select * from users where username ='"+userName+"' and password='"+password+"'";
		
		
		ResultSet result=null;
		try{
		Statement s= (Statement)connection.createStatement();

		result=(ResultSet)s.executeQuery(query);
		

		
		}catch(Exception e){
			
			System.out.println("Error query DBConn");
		}
		

	return result;
}
        
        
        public int insertUser(Connection connection,String username, String password) throws SQLException {
	Statement st = (Statement) this.conn.createStatement();
	st.execute("insert into users(username, password, name, address, cnp, isAdmin)" +
			" values ('"+ username+"','" + password +"','a','a','a',"+0+")", Statement.RETURN_GENERATED_KEYS);
	ResultSet generated = st.getGeneratedKeys();
	if (generated.next()) {
		return 1;
	}
	return -1;
}
    public ResultSet getAllUsers(Connection connection) {
	
	ResultSet resultSet = null;
	
	
	try {
		Statement statement = (Statement) connection.createStatement(
				ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY,
				ResultSet.HOLD_CURSORS_OVER_COMMIT);

		

		String selectAll = "Select * from users";

		resultSet = statement.executeQuery(selectAll);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
	
	return resultSet;
}
}
