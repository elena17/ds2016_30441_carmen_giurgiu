
package data;


import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

import com.mysql.jdbc.Connection;
import java.util.ArrayList;


public class UserBusiness {
    
    private UserDao userDao;
    
    public UserBusiness() {
		
		this.userDao = UserDao.getInstance();
	}
	
	public User getUser(String userName,String password){
		
		User user = null;
		Connection conn = null;
		
		conn = UserDao.connect();
		
		ResultSet resultSet = userDao.getUser(conn,userName,password);
		
		if (resultSet != null) {
			try {
				while (resultSet.next()) {
					user = new User(resultSet.getInt("id"),resultSet.getString("username"),resultSet.getString("password"),resultSet.getBoolean("isAdmin"));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		return user;
	}
        
        public int insertUser(String username, String password){
	int res = 0;
	Connection conn = null;	
	conn = userDao.connect();
	try {
		res = userDao.insertUser(conn, username, password);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return res;
}
        
        public ArrayList<User> getAllUsers(){
		User u = null;
		ArrayList<User> users = new ArrayList<User>();
		Connection conn = null;	
		conn = userDao.connect();
		ResultSet resultSet = userDao.getAllUsers(conn);
		
		try {
			while (resultSet.next()) {
                              
				u = new User(resultSet.getInt("id"),resultSet.getString("username"),resultSet.getString("password"),resultSet.getBoolean("isAdmin"));
                                if (! u.isIsAdmin()){
                                     users.add(u);
                                }
                               
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return users;
	}
    
}
