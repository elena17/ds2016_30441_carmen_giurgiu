package data;

import java.util.ArrayList;
import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Package;

public class PackageBusiness {
    
     private PackageDao pDao;
    
    public PackageBusiness() {
		
		this.pDao = PackageDao.getInstance();
	}
    
    public ArrayList<model.Package> getAllPackages(){
		model.Package u = null;
		ArrayList<Package> packages = new ArrayList<Package>();
		Connection conn = null;	
		conn = pDao.connect();
		ResultSet resultSet = pDao.getAllPackages(conn);
		
		try {
			while (resultSet.next()) {
                           
				u = new model.Package(resultSet.getInt("id"),resultSet.getString("name"),resultSet.getString("status"),resultSet.getString("history"),resultSet.getInt("weight"),resultSet.getBoolean("isTaken"));
				packages.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return packages;
	}
   
    public model.Package getPackageById(int id) {
        model.Package u = null;
        Connection conn = null;	
		conn = pDao.connect();
		ResultSet resultSet = pDao.getPackageById(conn, id);
        try {
			while (resultSet.next()) {
                           
				u = new model.Package(resultSet.getInt("id"),resultSet.getString("name"),resultSet.getString("status"),resultSet.getString("history"),resultSet.getInt("weight"),resultSet.getBoolean("isTaken"));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
        
        return u;
    }
    
    public int insertUserPackage(int userId, int packageId) {
        Connection conn = null;	
		conn = pDao.connect();
                int res = 0;
         try {
             res = pDao.insertUserPackage(conn, userId, packageId);
             System.out.println("* "+res);
         } catch (SQLException ex) {
             Logger.getLogger(PackageBusiness.class.getName()).log(Level.SEVERE, null, ex);
         }
                return res;
    }
    
    public void updatePackageTaken(int pId) {
        Connection conn = null;	
		conn = pDao.connect();
         pDao.updatePackageTaken(conn, pId);
    
    }
    
    public void updatePackageStatus(int pId,String status) {
        Connection conn = null;	
		conn = pDao.connect();
         pDao.updatePackageStatus(conn, pId, status);
    
    }
    
    
     public ArrayList<model.Package> getAllUserPackages(int userId){
		model.Package u = null;
		ArrayList<Package> packages = new ArrayList<Package>();
		Connection conn = null;	
		conn = pDao.connect();
		ResultSet resultSet = pDao.getPackagesByUserId(conn, userId);
		
		try {
			while (resultSet.next()) {
                           int pId = resultSet.getInt("packageId");
			   u = getPackageById(pId);
			   packages.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return packages;
	}
}
