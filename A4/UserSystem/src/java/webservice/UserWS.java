
package webservice;
import data.UserBusiness;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import model.User;


@WebService(serviceName = "UserWS")
public class UserWS {
    
     @WebMethod
    /**
     * returns true if the login is correct
     */
    public boolean loginUser(String username, String pass){
        User usr = null;
        UserBusiness ub = new UserBusiness();
        try {
            usr = ub.getUser(username, pass);
        } catch (Exception ex) {}
        if(usr != null)
            return true;
        else
            return false;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "loginUserGetUser")
    public User loginUserGetUser(@WebParam(name = "username") String username, @WebParam(name = "password") String password) {
        //TODO write your implementation code here:
        User usr = null;
        UserBusiness ub = new UserBusiness();
        try {
            usr = ub.getUser(username, password);
        } catch (Exception ex) {}
        
        return usr;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "registerUser")
    public int registerUser(@WebParam(name = "username") String username, @WebParam(name = "password") String password) {
        //TODO write your implementation code here:
        
        UserBusiness ub = new UserBusiness();
        int res = 0;
        res = ub.insertUser(username, password);
        return res;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getAllUsers")
    public ArrayList<model.User> getAllUsers() {
        //TODO write your implementation code here:
        UserBusiness ub = new UserBusiness();
        ArrayList<model.User> us = new ArrayList<model.User>();
        us = ub.getAllUsers();
        return us;
    }
}
