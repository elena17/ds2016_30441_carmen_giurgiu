
package webservice;

import data.PackageBusiness;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;


@WebService(serviceName = "PackageWS")
public class PackageWS {

   

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getAllPackages")
    public ArrayList <model.Package> getAllPackages() {
        
        ArrayList <model.Package> res = new ArrayList<model.Package>();
        PackageBusiness pbus = new PackageBusiness();
        res = pbus.getAllPackages();
        return res;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "assignPackage")
    public int assignPackage(@WebParam(name = "userId") int userId, @WebParam(name = "packageId") int packageId) {
        //TODO write your implementation code here:
        PackageBusiness pbus = new PackageBusiness();
        int res = pbus.insertUserPackage(userId, packageId);
        System.out.println("*** "+res);
        pbus.updatePackageTaken(packageId);
        return res;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "updatePackage")
    public int updatePackage(@WebParam(name = "packageId") int packageId, @WebParam(name = "status") String status) {
        //TODO write your implementation code here:
         PackageBusiness pbus = new PackageBusiness();
        pbus.updatePackageStatus(packageId,status);
        return 0;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getPackagesForUser")
    public ArrayList<model.Package> getPackagesForUser(@WebParam(name = "userId") int userId) {
        //TODO write your implementation code here:
        ArrayList <model.Package> res = new ArrayList<model.Package>();
        PackageBusiness pbus = new PackageBusiness();
        res = pbus.getAllUserPackages(userId);
        return res;
    }
}
