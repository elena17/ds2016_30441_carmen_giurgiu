
package model;


public class Package {
    
    private int id;
    private String name;
    private String status;
    private String history;
    private int weight;
    private boolean isTaken;
    
    public Package () {};

    public Package(int id, String name, String status, String history, int weight, boolean isTaken) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.history = history;
        this.weight = weight;
        this.isTaken = isTaken;
    }

    public Package(int id, String name, String status, String history, int weight) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.history = history;
        this.weight = weight;
    }

    public Package(String name, String status, String history, int weight) {
        this.name = name;
        this.status = status;
        this.history = history;
        this.weight = weight;
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public boolean isIsTaken() {
        return isTaken;
    }

    public void setIsTaken(boolean isTaken) {
        this.isTaken = isTaken;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "("+this.id+")";
    }
    
    
    
}
