package model;


public class User {
    
    private String username;
	private String password;
        private int userId;
        private boolean isAdmin;
	
    public User() {
		
	}

    public User(int id, String username, String password, boolean admin) {
        this.userId = id;
        this.username = username;
        this.password = password;
        this.isAdmin = admin;
                
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
     public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
}
