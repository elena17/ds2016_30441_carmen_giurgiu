package ds.common.classes.entities;

/**
 *
 * @author Giurgiu Carmen
 */
public class Car {
    
    private int year;
    private int engineSize;
    private double putchasingPrice;
    
    public Car(){}
    
    public Car(int year, int engineSize, double purchasingPrice){
        this.year = year;
        this.engineSize = engineSize;
        this.putchasingPrice = purchasingPrice;
        
    }
    
    
    public int getYear(){
        return this.year;
    }
    
    public void setYear(int year){
        this.year = year;
    }
    
    public int getEngineSize(){
        return this.engineSize;
    }
    
    public void setEngineSize(int engineSize){
        this.engineSize = engineSize;
    }
    
    public double getPurchasingPrice(){
        return this.putchasingPrice;
    }
    
    public void setPurchasingPrice(int purchasingPrice){
        this.putchasingPrice = purchasingPrice;
    }
}

