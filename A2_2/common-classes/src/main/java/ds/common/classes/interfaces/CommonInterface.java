package ds.common.classes.interfaces;

import ds.common.classes.entities.Car;
import java.rmi.Remote;

/**
 *
 * @author Giurgiu Carmen
 */
public interface CommonInterface  extends Remote{
    
    public double computeTax(Car c);
    double computeSellingPrice(Car c);
}
