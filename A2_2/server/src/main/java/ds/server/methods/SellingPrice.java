package ds.server.methods;

import ds.common.classes.entities.Car;
import ds.server.interfaces.ISellingPriceCalculator;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Giurgiu Carmen
 */
public abstract class SellingPrice extends UnicastRemoteObject implements ISellingPriceCalculator {
    
    public SellingPrice()  throws RemoteException{        
    } 
    
    @Override
    public double computeSellingPrice(Car c){
        double priceSelling = c.getPurchasingPrice() - (c.getPurchasingPrice()/7.0) * (2015.0 - c.getYear());
		
		return priceSelling;
    }
}