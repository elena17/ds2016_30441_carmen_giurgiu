package ds.server.methods;

import ds.common.classes.entities.Car;
import ds.server.interfaces.ITaxCalculator;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Giurgiu Carmen
 */
public abstract class TaxCalculator extends UnicastRemoteObject implements ITaxCalculator {
    
    public TaxCalculator()  throws RemoteException{        
    }
    
    @Override
    public double computeTax(Car c){
        
        int sum = 8;
        if (c.getEngineSize() <= 0) {
			throw new IllegalArgumentException("Engine size must be positive.");
		}
        if (c.getEngineSize() >= 1601) sum = 18;
        if (c.getEngineSize() >= 2001) sum = 72;
        if (c.getEngineSize() >= 2601) sum = 144;
        if (c.getEngineSize() >= 3001) sum = 290;
       
        return (c.getEngineSize()/200)*sum;
    }
    
}