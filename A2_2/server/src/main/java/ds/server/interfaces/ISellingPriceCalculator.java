package ds.server.interfaces;

import ds.common.classes.entities.Car;
import ds.common.classes.interfaces.CommonInterface;

/**
 *
 * @author Giurgiu Carmen
 */
public interface ISellingPriceCalculator extends CommonInterface {
    public double priceSelling(Car c);
}
