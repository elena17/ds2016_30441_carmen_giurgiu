package ds.server.interfaces;

import ds.common.classes.entities.Car;
import ds.common.classes.interfaces.CommonInterface;

/**
 *
 * @author Giurgiu Carmen
 */
public interface ITaxCalculator extends CommonInterface{
    public double computeTax(Car c);
}
