package ds.server.start;

import ds.common.classes.entities.Car;
import ds.server.methods.TaxCalculator;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author Giurgiu Carmen
 */
public class StartServer {
    private void startServer(){
       
        try {
            // create on port 1099
            Registry registry = LocateRegistry.createRegistry(1099);
             
            // create a new service named myMessage
            registry.rebind("taxCalculator", new TaxCalculator());
        } catch (Exception e) {
            e.printStackTrace();
        }     
        System.out.println("System is ready!");
    }
     
    public static void main(String[] args) {
        StartServer ss = new StartServer();
        ss.startServer();
    }
    
}
