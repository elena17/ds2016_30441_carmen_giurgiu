/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.client;

import ds.common.classes.entities.Car;
import ds.common.classes.interfaces.ITaxCalculator;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author vaio
 */
public class Test {
    private void doTest(){
        Car c = new Car(1992, 1890, 456.89);
        try {
            // fire to localhost port 1099
            Registry myRegistry = LocateRegistry.getRegistry("127.0.0.1", 1099);
             
            // search for myMessage service
            ITaxCalculator tax = (ITaxCalculator) myRegistry.lookup("taxCalculator");
             
            // call server's method        
           tax.computeTax(c);
             
            System.out.println("Message Sent");
        } catch (Exception e) {
            e.printStackTrace();
        }       
    }
     
    public static void main(String[] args) {
        Test main = new Test();
        main.doTest();
    }
    
}
