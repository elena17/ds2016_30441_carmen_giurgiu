package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellingService;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;

public class SellingService implements ISellingService {

	@Override
	public double computeSellingPrice(Car c) {
		
		double priceSelling = c.getPrice() - (c.getPrice()/7.0) * (2015.0 - c.getYear());
		
		return priceSelling;
	}

}
