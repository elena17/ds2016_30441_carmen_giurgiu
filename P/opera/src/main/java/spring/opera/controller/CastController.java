package spring.opera.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.opera.dto.CastDTO;
import spring.opera.services.CastService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/cast")
public class CastController {
    @Autowired
	private CastService userService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public CastDTO getUserById(@PathVariable("id") int id) {
		return userService.finCastById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<CastDTO> getAllCasts() {
		return userService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertCast(@RequestBody CastDTO castDTO) {
		return userService.create(castDTO);
	}
}
