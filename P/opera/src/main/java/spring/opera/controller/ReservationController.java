package spring.opera.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.opera.dto.ReservationDTO;
import spring.opera.services.ReservationService;



@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/reservation")
public class ReservationController {
    @Autowired
	private ReservationService resService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public ReservationDTO getUserById(@PathVariable("id") int id) {
		return resService.findReservationById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ReservationDTO> getAllUsers() {
		return resService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertUser(@RequestBody ReservationDTO resDTO) {
		return resService.create(resDTO);
	}
}
