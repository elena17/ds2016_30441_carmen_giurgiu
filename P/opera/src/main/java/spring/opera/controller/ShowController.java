package spring.opera.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.opera.dto.ShowDTO;
import spring.opera.services.ShowService;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/show")
public class ShowController {
    @Autowired
	private ShowService showService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public ShowDTO getUserById(@PathVariable("id") int id) {
		return showService.findShowById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ShowDTO> getAll() {
		return showService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertUser(@RequestBody ShowDTO showDTO) {
		return showService.create(showDTO);
	}
}
