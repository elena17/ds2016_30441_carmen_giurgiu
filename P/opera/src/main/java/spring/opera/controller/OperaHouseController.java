package spring.opera.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.opera.dto.OperaHouseDTO;
import spring.opera.services.OperaHouseService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/operahouse")
public class OperaHouseController {
    @Autowired
	private OperaHouseService operaService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public OperaHouseDTO getOperaHouseById(@PathVariable("id") int id) {
		return operaService.findOperaById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<OperaHouseDTO> getAll() {
		return operaService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertUser(@RequestBody OperaHouseDTO operaDTO) {
		return operaService.create(operaDTO);
	}
    
}
