package spring.opera.entities;



public class OperaHouse implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer noOfSeats;
	private String description;
	private String address;
        private String latitude;
        private String longitude;
	

	public OperaHouse() {
	}

	public OperaHouse(Integer id, String description, Integer noOfSeats, String address, String latitude, String longitude) {
		super();
		this.id = id; 
                this.description = description;
                this.noOfSeats = noOfSeats;
                this.address = address;
                this.latitude = latitude;
                this.longitude = longitude;
               
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
 
	public Integer getNoOfSeats() {
		return this.noOfSeats;
	}

	public void setNumberOfSeats(Integer noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
        

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
        
        
        
     
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
        
        
        
        
  
	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
        
        
        
   
	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
        }
        
}