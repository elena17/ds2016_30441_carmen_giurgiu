package spring.opera.entities;





public class Cast implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer idShow;
	private String name;
	private String character;
	

	public Cast() {
	}

	public Cast(Integer id, Integer idShow, String name, String character) {
		super();
		this.id = id;
                this.idShow = idShow;
		this.name = name;
		this.character = character;
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
        
      
	public int getIdShow() {
		return this.idShow;
	}

	public void setIdShow(int idShow) {
		this.idShow = idShow;
	}

	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCharacter() {
		return this.character;
	}

	public void setCharcter(String character) {
		this.character = character;
	}
}
	