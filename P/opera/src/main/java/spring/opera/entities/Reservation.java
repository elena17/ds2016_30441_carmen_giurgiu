
package spring.opera.entities;

import java.sql.Time;
import java.util.Date;


public class Reservation {
    private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer idClient;
        private Integer idShow;
        private Integer idOpera;
	private Integer seatNr;
        private double price;
        private Date date;
        private Time time;
	

	public Reservation() {
	}

	public Reservation(Integer id, Integer idClient, Integer idShow, Integer idOpera, Integer seatNr, double price, Date date, Time time) {                                 
		super();
		this.id = id;
                this.idClient = idClient;
                this.idShow = idShow;
		this.idOpera = idOpera;
                this.price = price;
                this.seatNr = seatNr;
                this.time = time;
                this.date = date;
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
      public Integer getIdClient() {
		return this.idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}  
      
	public int getIdShow() {
		return this.idShow;
	}

	public void setIdShow(int idShow) {
		this.idShow = idShow;
	}

        
        public Integer getIdOpera() {
		return this.idOpera;
	}

	public void setIdOpera(Integer id) {
		this.idOpera = id;
	}
        
        
        public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
        
        public Integer getSeatNr() {
		return this.seatNr;
	}

	public void setSeatNr(Integer seatNr) {
		this.seatNr = seatNr;
	}
        
        public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
        
        
        public Time getTime() {
		return this.time;
	}

	public void setTime(Time time) {
		this.time = time;
	}
	
}
