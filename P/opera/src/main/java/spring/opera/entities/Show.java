package spring.opera.entities;

public class Show implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer idOpera;
	private String name;
	private String author;
        private String description;
	

	public Show() {
	}

	public Show(Integer id, Integer idOpera, String name, String author, String description) {
		super();
		this.id = id;
                this.idOpera = idOpera;
		this.name = name;
		this.author = author;
                this.description = description;
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
        
       
	public int getIdOpera() {
		return this.idOpera;
	}

	public void setIdOpera(int idOpera) {
		this.idOpera = idOpera;
	}

	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
        
        
        
        
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

