package spring.opera.dto;


public class OperaHouseDTO {
   private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer noOfSeats;
	private String description;
	private String address;
        private String latitude;
        private String longitude;
	

	public OperaHouseDTO() {
	}

	public OperaHouseDTO(Integer id, String description, Integer noOfSeats, String address, String latitude, String longitude) {
		super();
		this.id = id; 
                this.description = description;
                this.noOfSeats = noOfSeats;
                this.address = address;
                this.latitude = latitude;
                this.longitude = longitude;
               
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
 
	public Integer getNoOfSeats() {
		return this.noOfSeats;
	}

	public void setNumberOfSeats(Integer noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
        

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
        
        
        
     
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
        
        
        
        
  
	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
        
        
        
   
	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
        }
        
        
         public static class Builder {
		private Integer nestedid;
                private Integer nestednoofseats;
                private String nesteddescription;
                private String nestedaddress;
                private String nestedlatitude;
                private String nestedlongitude;


		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
                
                public Builder noOfSeats(int no) {
			this.nestednoofseats = no;
			return this;
		}

		
		public Builder description(String des) {
			this.nesteddescription = des;
			return this;
		}

                
                public Builder address(String addr) {
			this.nestedaddress = addr;
			return this;
		}
		
		public Builder latitude(String lat) {
			this.nestedlatitude = lat;
			return this;
		}
                
                public Builder longitude(String lon) {
			this.nestedlongitude = lon;
			return this;
		}
		
		



		public OperaHouseDTO create() {
			return new OperaHouseDTO(nestedid, nesteddescription, nestednoofseats,  nestedaddress, nestedlatitude, nestedlongitude);
		}
                
         }
         
}
