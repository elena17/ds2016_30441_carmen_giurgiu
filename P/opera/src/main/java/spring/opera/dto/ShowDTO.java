package spring.opera.dto;


public class ShowDTO {
    
	private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer idOpera;
	private String name;
	private String author;
        private String description;
	

	public ShowDTO() {
	}

	public ShowDTO(Integer id, Integer idOpera, String name, String author, String description) {
		super();
		this.id = id;
                this.idOpera = idOpera;
		this.name = name;
		this.author = author;
                this.description = description;
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
        
       
	public int getIdOpera() {
		return this.idOpera;
	}

	public void setIdOpera(int idOpera) {
		this.idOpera = idOpera;
	}

	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
        
        
        
        
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
        
        
    public static class Builder {
		private Integer nestedid;
                private Integer nestedidOpera;
		private String nestedname;
		private String nestedauthor;
		private String nesteddescription;
		

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
                
                public Builder idOpera(int id) {
			this.nestedidOpera = id;
			return this;
		}

		public Builder name(String name) {
			this.nestedname = name;
			return this;
		}
		
		public Builder author(String author) {
			this.nestedauthor = author;
			return this;
		}

		public Builder description(String email) {
			this.nesteddescription = email;
			return this;
		}

		



		public ShowDTO create() {
			return new ShowDTO(nestedid, nestedidOpera,nestedname, nestedauthor, nesteddescription);
		}

	}
}
