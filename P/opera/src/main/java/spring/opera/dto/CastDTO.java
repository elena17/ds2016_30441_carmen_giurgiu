package spring.opera.dto;

/**
 *
 * @author vaio
 */
public class CastDTO {
    private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer idShow;
	private String name;
	private String character;
	

	public CastDTO() {
	}

	public CastDTO(Integer id, Integer idShow, String name, String character) {
		super();
		this.id = id;
                this.idShow = idShow;
		this.name = name;
		this.character = character;
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
        
      
	public int getIdShow() {
		return this.idShow;
	}

	public void setIdShow(int idShow) {
		this.idShow = idShow;
	}

	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCharacter() {
		return this.character;
	}

	public void setCharcter(String character) {
		this.character = character;
	}
        
        
        
        public static class Builder {
		private Integer nestedid;
                private Integer nestedidshow;
                private String nestedname;
                private String nestedcharacter;

		

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
                
                public Builder idShow(int id) {
			this.nestedidshow = id;
			return this;
		}

		public Builder name(String name) {
			this.nestedname = name;
			return this;
		}
		
		public Builder character(String ch) {
			this.nestedcharacter = ch;
			return this;
		}

			



		public CastDTO create() {
			return new CastDTO(nestedid, nestedidshow,nestedname, nestedcharacter);
		}

	}
        
        
}
