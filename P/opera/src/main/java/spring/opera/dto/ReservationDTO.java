package spring.opera.dto;

import java.sql.Time;
import java.util.Date;


public class ReservationDTO {
     private static final long serialVersionUID = 1L;
	private Integer id;
        private Integer idClient;
        private Integer idShow;
        private Integer idOpera;
	private Integer seatNr;
        private double price;
        private Date date;
        private Time time;
	

	public ReservationDTO() {
	}

	public ReservationDTO(Integer id, Integer idClient, Integer idShow, Integer idOpera, Integer seatNr, double price, Date date, Time time) {                                 
		super();
		this.id = id;
                this.idClient = idClient;
                this.idShow = idShow;
		this.idOpera = idOpera;
                this.price = price;
                this.seatNr = seatNr;
                this.time = time;
                this.date = date;
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
        
      public Integer getIdClient() {
		return this.idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}  
      
	public int getIdShow() {
		return this.idShow;
	}

	public void setIdShow(int idShow) {
		this.idShow = idShow;
	}

        
        public Integer getIdOpera() {
		return this.idOpera;
	}

	public void setIdOpera(Integer id) {
		this.idOpera = id;
	}
        
        
        public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
        
        public Integer getSeatNr() {
		return this.seatNr;
	}

	public void setSeatNr(Integer seatNr) {
		this.seatNr = seatNr;
	}
        
        public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
        
        
        public Time getTime() {
		return this.time;
	}

	public void setTime(Time time) {
		this.time = time;
	}
        
        
        
        public static class Builder {
		private Integer nestedid;
                private Integer nestedidclient;
                private Integer nestedidshow;
                private Integer nestedidOpera;
                private Integer nestedseatnr;
                private double nestedprice;
		private Date nesteddate;
                private Time nestedtime;
		

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
                
                public Builder idClient(int id) {
			this.nestedidclient = id;
			return this;
		}
                
                
                public Builder idShow(int id) {
			this.nestedidshow = id;
			return this;
		}
                
                public Builder idOpera(int id) {
			this.nestedidOpera = id;
			return this;
		}
                
                public Builder seatNr(int nr) {
			this.nestedseatnr = nr;
			return this;
		}

				
		public Builder price(double p) {
			this.nestedprice = p;
			return this;
		}

		public Builder date(Date d) {
			this.nesteddate = d;
			return this;
		}
                
                
                public Builder time(Time t) {
			this.nestedtime = t;
			return this;
		}


		



		public ReservationDTO create() {
			return new ReservationDTO(nestedid, nestedidclient, nestedidshow, nestedidOpera, nestedseatnr,nestedprice, nesteddate, nestedtime);
		}

	}
	
}
