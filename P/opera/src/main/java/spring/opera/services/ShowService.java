package spring.opera.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import spring.opera.dto.ShowDTO;
import spring.opera.entities.Show;
import spring.opera.errorhandler.ResourceNotFoundException;
import spring.opera.repositories.ShowRepository;

public class ShowService {
    @Autowired
	private ShowRepository showRepository;

	public ShowDTO findShowById(int userId) {
		Show show = showRepository.findById(userId);
		if (show == null) {
			throw new ResourceNotFoundException(Show.class.getSimpleName());
		}
		

		ShowDTO dto = new ShowDTO.Builder()
						.id(show.getId())
						.idOpera(show.getIdOpera())
						.name(show.getName())
						.author(show.getAuthor())
						.description(show.getDescription())
						.create();
		return dto;
	}
	
	public List<ShowDTO> findAll() {
		List<Show> shows = showRepository.findAll();
		List<ShowDTO> toReturn = new ArrayList<ShowDTO>();
		for (Show show : shows) {
			
			ShowDTO dto = new ShowDTO.Builder()
						.id(show.getId())
						.idOpera(show.getIdOpera())
						.name(show.getName())
						.author(show.getAuthor())
						.description(show.getDescription())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(ShowDTO showDTO) {
		Show show = new Show();
                show.setId(showDTO.getId());
                show.setIdOpera(showDTO.getIdOpera());
		show.setName(showDTO.getName());
		show.setAuthor(showDTO.getAuthor());
		show.setDescription(showDTO.getDescription());
		
		


		Show sh = showRepository.save(show);
		return sh.getId();
	}

	
    
}
