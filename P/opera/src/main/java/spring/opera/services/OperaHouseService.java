package spring.opera.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import spring.opera.dto.OperaHouseDTO;
import spring.opera.entities.OperaHouse;
import spring.opera.errorhandler.ResourceNotFoundException;
import spring.opera.repositories.OperaHouseRepository;


public class OperaHouseService {
    
    
     @Autowired
	private OperaHouseRepository operaRepository;

	public OperaHouseDTO findOperaById(int userId) {
		OperaHouse oh = operaRepository.findById(userId);
		if (oh == null) {
			throw new ResourceNotFoundException(OperaHouse.class.getSimpleName());
		}
		

		OperaHouseDTO dto = new OperaHouseDTO.Builder()
						.id(oh.getId())
                                                .description(oh.getDescription())
						.noOfSeats(oh.getNoOfSeats())
						.address(oh.getAddress())
						.latitude(oh.getLatitude())
						.longitude(oh.getLongitude())
						.create();
		return dto;
	}
	
	public List<OperaHouseDTO> findAll() {
		List<OperaHouse> ohs = operaRepository.findAll();
		List<OperaHouseDTO> toReturn = new ArrayList<OperaHouseDTO>();
		for (OperaHouse oh : ohs) {
			
			OperaHouseDTO dto = new OperaHouseDTO.Builder()
						.id(oh.getId())
                                                .description(oh.getDescription())
						.noOfSeats(oh.getNoOfSeats())
						.address(oh.getAddress())
						.latitude(oh.getLatitude())
						.longitude(oh.getLongitude())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(OperaHouseDTO operaDTO) {
		OperaHouse oh = new OperaHouse();
                oh.setId(operaDTO.getId());
                oh.setDescription(operaDTO.getDescription());
                oh.setNumberOfSeats(operaDTO.getNoOfSeats());
                oh.setAddress(operaDTO.getAddress());
                oh.setLatitude(operaDTO.getLatitude());
                oh.setLongitude(operaDTO.getLongitude());
		
		


		OperaHouse operaHouse = operaRepository.save(oh);
		return operaHouse.getId();
	}

	
    
}
