package spring.opera.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import spring.opera.dto.CastDTO;
import spring.opera.entities.Cast;
import spring.opera.errorhandler.ResourceNotFoundException;
import spring.opera.repositories.CastRepository;

public class CastService {
    @Autowired
	private CastRepository castRepository;

	public CastDTO finCastById(int castId) {
		Cast cast = castRepository.findById(castId);
		if (cast == null) {
			throw new ResourceNotFoundException(Cast.class.getSimpleName());
		}
		

		CastDTO dto = new CastDTO.Builder()
						.id(cast.getId())
						.idShow(cast.getIdShow())
						.name(cast.getName())
						.character(cast.getCharacter())						
						.create();
		return dto;
	}
	
	public List<CastDTO> findAll() {
		List<Cast> casts = castRepository.findAll();
		List<CastDTO> toReturn = new ArrayList<CastDTO>();
		for (Cast cast : casts) {
			
			CastDTO dto = new CastDTO.Builder()
						.id(cast.getId())
						.idShow(cast.getIdShow())
						.name(cast.getName())
						.character(cast.getCharacter())						
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(CastDTO castDTO) {
		Cast cast = new Cast();
                cast.setId(castDTO.getId());
                cast.setIdShow(castDTO.getIdShow());
		cast.setName(castDTO.getName());
		cast.setCharcter(castDTO.getCharacter());
		
		
		


		Cast ca = castRepository.save(cast);
		return ca.getId();
}
        
}
