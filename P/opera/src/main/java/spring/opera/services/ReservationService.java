package spring.opera.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import spring.opera.dto.ReservationDTO;
import spring.opera.entities.Reservation;
import spring.opera.errorhandler.ResourceNotFoundException;
import spring.opera.repositories.ReservationRepository;



public class ReservationService {
    @Autowired
	private ReservationRepository resRepository;

	public ReservationDTO findReservationById(int resId) {
		Reservation res = resRepository.findById(resId);
		if (res == null) {
			throw new ResourceNotFoundException(Reservation.class.getSimpleName());
		}
		

		ReservationDTO dto = new ReservationDTO.Builder()
						.id(res.getId())
                                                .idClient(res.getIdClient())
                                                .idShow(res.getIdShow())
						.idOpera(res.getIdOpera())
						.seatNr(res.getSeatNr())
                                                .price(res.getPrice())
						.date(res.getDate())
						.time(res.getTime())
						.create();
		return dto;
	}
	
	public List<ReservationDTO> findAll() {
		List<Reservation> reservations = resRepository.findAll();
		List<ReservationDTO> toReturn = new ArrayList<ReservationDTO>();
		for (Reservation res : reservations) {
			
			ReservationDTO dto = new ReservationDTO.Builder()
						.id(res.getId())
                                                .idClient(res.getIdClient())
                                                .idShow(res.getIdShow())
						.idOpera(res.getIdOpera())
						.seatNr(res.getSeatNr())
                                                .price(res.getPrice())
						.date(res.getDate())
						.time(res.getTime())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(ReservationDTO resDTO) {
		Reservation res = new Reservation();
                res.setId(resDTO.getId());
                res.setIdClient(resDTO.getIdClient());
                res.setIdShow(resDTO.getIdShow());
                res.setIdOpera(resDTO.getIdOpera());
                res.setSeatNr(resDTO.getSeatNr());
                res.setPrice(resDTO.getPrice());               
                res.setDate(resDTO.getDate());
                res.setTime(resDTO.getTime());
		
		


		Reservation reservation = resRepository.save(res);
		return reservation.getId();
	}

}
