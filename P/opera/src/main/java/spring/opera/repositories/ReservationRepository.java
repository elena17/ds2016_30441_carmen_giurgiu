package spring.opera.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.opera.entities.Reservation;


public interface ReservationRepository extends JpaRepository<Reservation, Integer>{
        Reservation findByName(String name);

	Reservation findById(int id);
}
