
package spring.opera.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.opera.entities.OperaHouse;


public interface OperaHouseRepository extends JpaRepository<OperaHouse, Integer> {

	OperaHouse findByName(String name);

	OperaHouse findById(int id);
}
