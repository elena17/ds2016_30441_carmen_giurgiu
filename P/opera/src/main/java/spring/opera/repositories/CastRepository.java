
package spring.opera.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.opera.entities.Cast;





public interface CastRepository extends JpaRepository<Cast, Integer> {

	Cast findByName(String name);

	Cast findById(int id);

}
