package spring.opera.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.opera.entities.Show;



public interface ShowRepository extends JpaRepository<Show, Integer> {
    
	Show findByName(String name);

	Show findById(int id);
}
